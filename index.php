<?php

/*
 *
 * Created by Marvin Roßkothen using PHP Storm
 *
 */

require_once('src/Frameworks/FPDF/fpdf.php');

class PDF extends FPDF
{
    // Header
    function Header()
    {
        $this->SetFont('Arial', 'B', 16);
        $this->Cell(30, 10, 'Marvin PDF');
        $this->SetFont('Arial', '', 14);
        $this->Cell(5);
        $this->Cell(30, 10, '(PDF erstellt um ' . date('H:m:s') . " Uhr)");

        $this->Ln(20);
    }

    // Footer
    function Footer()
    {
        // 1.5cm vom unteren Rand entfernt
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);

        // Seitenzahl
        $this->Cell(0, 10, 'Seite ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }
}

// CONFIGURATION
$lineHeight = 11;
// CONFIGURATION END

// SETUP
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddFont('Open Sans');


$pdf->AddPage();
$pdf->SetFont('Open Sans', '', 10);

$messages = array(
    "Diese PDF wird durch einen for-Loop beschrieben; das heißt, dass diese Nachrichten",
    "in einem Array gespeichert, und durch einen for-Loop per \$pdf->Cell() ausgegeben werden.",
    "Das mag zwar nicht die sinnvollste Methodik sein, allerdings beinhält diese PDF bisher nur Text, daher",
    "geht das für diesen Zweck erstmal klar."
);

for ($i = 0; $i < sizeof($messages); $i++) {
    $pdf->Cell(0, $lineHeight / 2.25, iconv('UTF-8', 'windows-1252', $messages[$i]), 0, 1); // iconv bewirkt, dass alle Nachrichten in UTF-8 ausgegeben werden (mit ä, ö, ü, usw.)
}

$pdf->Output();
?>